# ovh-dns-change-ip
Python Script to automaticaly change IP of a DNS A record for a OVH 
Can update root Domain Record or SubDomain Record

# Setup
#### 1 - Create Application for interact with OVH
Go inside the corresponding URL and create an application to interact with your OVH Account:

  - [OVH Europe]
  - [OVH US]
  - [OVH North-America]
  - [So you Start Europe]
  - [So you Start North America]
  - [Kimsufi Europe]
  - [Kimsufi North America]


   [OVH Europe]: <https://eu.api.ovh.com/createApp/>
   [OVH US]: <https://api.us.ovhcloud.com/createApp/> 
   [OVH North-America]: <https://ca.api.ovh.com/createApp/>
   [So you Start Europe]: <https://eu.api.soyoustart.com/createApp/>
   [So you Start North America]: <https://ca.api.soyoustart.com/createApp/>
   [Kimsufi Europe]: <https://eu.api.kimsufi.com/createApp/>
   [Kimsufi North America]: <https://ca.api.kimsufi.com/createApp/>
   
#### 2 - Fill ovh-dns-change-ip.conf file
Complete the file `.env` with your Endpoint, Application Key, Secret and the URL of your Domain and SubDomain (if neccesary)

Build:
```
docker build -t ovh-dns .
```

Configure:
```
# edit env-example.com
docker run -it --rm -v ${PWD}/conf/:/app/conf/ --env-file ./env-example.com ovh-dns /bin/bash
# in the interactive shell do
# python3 ovh-dns-change-ip.py
```

Update dns entries:
```
docker compose --env-file env-example.com up
```

