from configparser import ConfigParser
import os
import sys
import requests
import ovh

ovh_conf = os.environ["OVH_CONF"]

#Retrieve IP 
IPCurrent=requests.get("https://ifconfig.me").text

def previous_ip_is_current():
    if not os.path.isfile(os.path.join("conf", "{}-previous_ip".format(ovh_conf))):
        return False
    with open(os.path.join("conf", "{}-previous_ip".format(ovh_conf)), 'r') as previous_ip:
        if previous_ip.readlines()[0].strip() != IPCurrent:
            return False
    return True

def persist_current_ip():
    with open(os.path.join("conf", "{}-previous_ip".format(ovh_conf)), 'w') as previous_ip:
        previous_ip.write(IPCurrent)


def setup_ovh():
    endpoint = os.environ["OVH_ENDPOINT"]
    application_key = os.environ["OVH_APPLICATION_KEY"]
    application_secret = os.environ["OVH_APPLICATION_SECRET"]

    ###Retrieve the Customer Key
    client = ovh.Client(
        endpoint=endpoint,
        application_key=application_key,
        application_secret=application_secret
    )
    # Request RO, /me API access
    ck = client.new_consumer_key_request()
    ck.add_rules(ovh.API_READ_ONLY, "/domain/zone/*/record")
    ck.add_rules(ovh.API_READ_WRITE_SAFE, "/domain/zone/*/record/*")
    ck.add_rules(ovh.API_READ_WRITE_SAFE, "/domain/zone/*/refresh")


    # Request token
    validation = ck.request()
    print("You need to verify and accept the permission to allow the application to use the OVH APIs")
    print("Please visit %s to authenticate" % validation['validationUrl'])
    input("then press Enter...")


    #Create ovh.conf file for ovh library https://github.com/ovh/python-ovh
    config_ovh = ConfigParser()
    config_ovh["default"]= {"endpoint":endpoint}
    config_ovh[endpoint]={}
    config_ovh[endpoint]["application_key"]=application_key
    config_ovh[endpoint]["application_secret"]=application_secret
    config_ovh[endpoint]["consumer_key"]=validation['consumerKey']

    with open(os.path.join("conf", ovh_conf), 'w') as configfile_ovh:
        config_ovh.write(configfile_ovh)





def ovh_call():
    domain=os.environ["OVH_DOMAIN"]
    sub_domains=os.environ["OVH_SUB_DOMAINS"]
    print(sub_domains)
    for sub_domain in sub_domains.split(","):
        #OVH call
        try:
            client = ovh.Client(config_file=os.path.join("conf", ovh_conf))
            ListRecord=client.get('/domain/zone/%s/record' % domain,
                                fieldType='A',
                                subDomain=sub_domain)
        
            if len(ListRecord) > 1:
                print("Error: We retrieved more than one record")
                print("We will NOT continue")
                print("List of Record:")
                i=1
                for record in ListRecord:
                    detailRecord=client.get('/domain/zone/%s/record/%s' % (domain,record))
                    print("Record %s :" % i)
                    print(detailRecord)
                os._exit(status=3)
        
            record=ListRecord[0]
            detailRecord=client.get('/domain/zone/%s/record/%s' % (domain,record))
            targetIpRecord=detailRecord["target"]
            print("IP :"+str(targetIpRecord))
            if targetIpRecord==IPCurrent:
                print("IP identical, record will not need to change")
            else:
                print("The local IP is not the DNS IP, we will change the Record")
                detailRecord=client.put('/domain/zone/%s/record/%s' % (domain,record),target=IPCurrent)
                print("IP Change OK")
                client.post('/domain/zone/%s/refresh'  % (domain))
                print("Refresh OK")
          
        except ovh.exceptions.NotGrantedCall:
            print("Permission denied, you need to setup again your grant was revoked")
            print("Execute the script with a --setup or -s")
        except Exception  as exception:
            print("ERROR")
            print(type(exception).__name__+" : "+str(exception))


if __name__=="__main__":
    #Check file ovh.conf exist
    if not previous_ip_is_current():
        if not os.path.isfile(os.path.join("conf", ovh_conf)):
            setup_ovh()
        else:
            ovh_call()
            persist_current_ip()
    else:
        print("Ip did not change")

